package com.jdbcpractice;

import java.sql.*;
import java.util.Scanner;

import static com.jdbcpractice.service.JdbcService.*;

public class Main {
    public static void main(String[] args) {
        Connection connection = connect();
        boolean isExited = false;
        while (!isExited){
            isExited = menu(connection);
        }

    }
    private static boolean menu(Connection connection){
        Scanner scanner = new Scanner(System.in);
        int options = 0;
        System.out.println("1-show all\t2-update\t3-delete\t4-insert\t5-exit");
        System.out.print("Please Enter 1-4 : ");
        try {
            options = scanner.nextInt();
        }catch (Exception e){
            System.out.println("Invalid input! ");
        }
        switch (options){
            case 1:
                findAll(connection);
                break;
            case 2:
                updateById(connection);
                break;
            case 3:
                deleteById(connection);
                break;
            case 4:
                saveById(connection);
            case 5:
                return true;
            default:
                System.out.println("Invalid input!");
                return false;
        }
        return true;
    }
}
